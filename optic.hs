{-# OPTIONS_GHC -Wall #-}
{-# OPTIONS_GHC -fno-warn-unused-imports #-}
{-# LANGUAGE RankNTypes #-}

import Data.Char(toUpper)
import Data.Functor.Identity

data Vehicle =
  Vehicle
    String -- make
    VehicleRegistration
  deriving (Eq, Show)
  
data VehicleRegistration =
  VehicleRegistration
    String -- reg number
    Person -- owner
  deriving (Eq, Show)
  
data Person =
  Person
    String -- name
    Int    -- age
  deriving (Eq, Show)

type Optic p f a b =
  p b (f b) -> p a (f a)

type Lens a b =
  forall f. Functor f => Optic (->) f a b

vehicleMake ::
  Lens Vehicle String
vehicleMake =
  error "todo: vehicleMake"

vehicleRegistration ::
  Lens Vehicle VehicleRegistration
vehicleRegistration =
  error "todo: vehicleRegistration"

registrationOwner ::
  Lens VehicleRegistration Person
registrationOwner =
  error "todo: registrationOwner"

-- Use: vehicleRegistration
--      registrationOwner
vehicleOwner ::
  Lens Vehicle Person
vehicleOwner =
  error "todo: vehicleOwner"

modify ::
  Optic (->) Identity a b
  -> (b -> b)
  -> a
  -> a
modify =
  error "todo: modify"

vehicleOwnerAge ::
  Lens Person Int
vehicleOwnerAge =
  error "todo: vehicleOwnerAge"

-- Update the age of the owner of a vehicle
--
-- use: modify, vehicleOwner, vehicleOwnerAge
birthday ::
  Vehicle
  -> Vehicle
birthday =
  error "todo: birthday"

data IntOrStringOrChar =
  IsInt Int
  | IsString String
  | IsChar Char
  deriving (Eq, Show)

class Profunctor p where
  dimap ::
    (a -> b)
    -> (c -> d)
    -> p b c
    -> p a d

instance Profunctor (->) where
  dimap =
    error "todo: Profunctor.dimap#(->)"

class Profunctor p => Choice p where
  left ::
    p a b
    -> p (Either a c) (Either b c)
  right ::
    p a b
    -> p (Either c a) (Either c b)

instance Choice (->) where
  left =
    error "todo: Choice.left#(->)"
  right =
    error "todo: Choice.right#(->)"

type Prism a b =
  forall p f. (Choice p, Applicative f) => Optic p f a b

makePrism ::
  (b -> a)
  -> (a -> Maybe b)
  -> Prism a b
makePrism f g =
  error "todo: makePrism"

-- use makePrism
_Int ::
  Prism IntOrStringOrChar Int
_Int =
  error "todo: _Int"

-- use modify and _Int
add1 ::
  IntOrStringOrChar
  -> IntOrStringOrChar
add1 =
  error "todo: add1"

type Traversal a b =
  forall f. Applicative f => Optic (->) f a b

data Client =
  Client
    String -- first name
    String -- surname
    Int -- client number
  deriving (Eq, Show)

listTraversal ::
  Traversal [a] a
listTraversal =
  error "todo: listTraversal"

-- traverse both first name and surname
clientNames ::
  Traversal Client String
clientNames =
  error "todo: clientName"

-- turn a client's first name and surname to all upper-case
-- Use:
--   modify
--   clientNames
--   listTraversal
--   toUpper
upperCase ::
  Client
  -> Client
upperCase =
  error "todo: upperCase"
